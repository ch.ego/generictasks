package ru.chelnokov.labs.first.classes;

public class XYZT extends Coordinate {
    int z;
    int t;
    public XYZT(int x, int y, int z, int t) {
        super(x, y);
        this.z = t;
        this.t = t;
    }
    @Override
    public String toString(){
        return  "("+ x + ";" + y + ";" + z + ";" + t +")";
    }
}
