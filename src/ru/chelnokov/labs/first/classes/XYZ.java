package ru.chelnokov.labs.first.classes;

public class XYZ extends Coordinate {
    int z;
    public XYZ(int x, int y, int z) {
        super(x, y);
        this.z = z;
    }

    @Override
    public String toString(){
        return "("+ x + ";" + y + ";" + z + ")";
    }
}
