package ru.chelnokov.labs.first;

import ru.chelnokov.labs.first.generics.CoordinateGen;
import ru.chelnokov.labs.first.classes.Coordinate;
import ru.chelnokov.labs.first.classes.XY;
import ru.chelnokov.labs.first.classes.XYZ;
import ru.chelnokov.labs.first.classes.XYZT;

public class Main {
    public static void main(String[] args) {
        Coordinate [] coordinates = {new XY(2,5), new XYZ(3,6, -2), new XYZT(6,3,-8,13)};
        CoordinateGen<Coordinate> coordinateGen = new CoordinateGen<>(coordinates);

        coordinateGen.printAllXY();
        coordinateGen.printAllXYZ();
    }
}
