package ru.chelnokov.labs.first.generics;

import ru.chelnokov.labs.first.classes.XY;
import ru.chelnokov.labs.first.classes.XYZ;

public class CoordinateGen<T> {
    private final T [] coordinates;

    public CoordinateGen(T[] coordinates) {
        this.coordinates = coordinates;
    }

    public void printAllXY(){
        System.out.println("===XY===");
        for (T coordinate : coordinates){
            if (coordinate.getClass() == XY.class){
                System.out.println(coordinate);
            }
        }
        System.out.println("========");
    }

    public void printAllXYZ(){
        System.out.println("===XYZ===");
        for (T coordinate : coordinates){
            if (coordinate.getClass() == XYZ.class){
                System.out.println(coordinate);
            }
        }
        System.out.println("========");
    }
}
